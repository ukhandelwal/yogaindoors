var express = require('express');
var app = express();
var dbhandler = require('./dbhandler.js');
var schedule = require("./models/schedule.js");
var feedback = require("./models/feedback.js");
var qexpert = require("./models/qexpert.js");
var Subscriber = require("./models/subscriber.js");
var CareerProfile = require("./models/career.js");
var path = require('path');
app.listen( process.env.PORT || 8080);

//app.listen(8125,"192.168.1.7");
app.set('views', __dirname + '/views');

app.set('view engine', 'ejs');

var parser = require("body-parser");
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.render('index');
});

app.get('/privacypolicy', function (req, res) {
    res.render('privacypolicy')
});

app.get('/refundpolicy', function (req, res) {
    res.render('refundpolicy')
});

app.post('/scheduleinfo', function (req, res) {
    var scheduleInfo = new schedule(req.body.schedule);
    dbhandler.putScheduleInfo(scheduleInfo, res);
});

app.post('/feedback', function (req, res) {
    console.log("email " +req.body.feedback.email);
    var feedbackInfo = new feedback(req.body.feedback);
    dbhandler.putFeedback(feedbackInfo, res);
});


app.post('/qExpert', function (req, res) {
    console.log("email " + req.body.qExpert.email);
    var qExpertInfo = new qexpert(req.body.qExpert);
    dbhandler.putQExpert(qExpertInfo, req.headers.host, res);
});


app.post('/qExpertSession', function (req, res) {
    var qExpertAnswer = new qexpert(req.body.qExpert);
    var qExpertQuestion = dbhandler.putqExpertAnswer(qExpertAnswer, res);
});

app.post('/subscribe', function (req, res) {
    var subscriber = new Subscriber(req.body.subscribe);
    dbhandler.putSubscribers(subscriber, res);
});

app.get('/qExpertAnswerForm/:id', function (req, res) {
   dbhandler.getQuestion(req.params.id, req.headers.host, res);
});


app.post('/careers', function (req, res) {
    var profile = new CareerProfile(req.body.careers);
    dbhandler.putCareerInfo(profile, res);
});