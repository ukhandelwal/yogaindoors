var db = require('mysql');
var sendgrid = require('./emailer.js');
var qexpert = require("./models/qexpert.js");
//var connection = db.createConnection({
//    host: 'localhost',
//    database: 'yogaindoors',
//    user: 'yogaindoorU0mv0',
//    password: 'hJ{@HFK747%$'

//});

var connection  = db.createConnection({
  host      : 'us-cdbr-azure-east-b.cloudapp.net',
  database  : 'Yogaindoors775',
  user: 'b03da5e9622cf1',
  password : '8ec895d9'

});
//var connection  = db.createConnection({
//  host      : 'mysql://$OPENSHIFT_MYSQL_DB_HOST:$OPENSHIFT_MYSQL_DB_PORT/',
//  database  : 'yogaindoors',
//  user: 'adminSFVf1NC',
//  password : 'iZXuURnNjfcq'

//});

exports.putScheduleInfo = function (schedule, res) {
    var strQuery = "Insert into schedules values ('" + schedule.useremail + "','" + schedule.time + "','" + schedule.locale + "', NULL)";
    connection.query(strQuery, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            sendgrid.sendTeamUserRegMail(schedule);
            sendgrid.sendRegistrationEmail(schedule.useremail, res);
        }
    });
}

exports.putFeedback = function (feedback, res) {
    var strQuery = "Insert into feedback values ('" + feedback.fullname + "','" + feedback.email + "','" + feedback.message + "', NULL)";
    connection.query(strQuery, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            res.render('index');
        }
    });
}


exports.putQExpert = function (qexpert, hostname, res) {
    var strQuery = "Insert into questionexpert SET ?";
    var post = { name: qexpert.name,
        email: qexpert.useremail,
        question: qexpert.question,
        qtime: qexpert.qtime
    };
    var query = connection.query(strQuery, post, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            var id = result.insertId;
            console.log("Result " + id);
            sendgrid.sendQuestionExpert(id, hostname, res);
        }
    });
}

exports.putSubscribers = function (subscriber, res) {
    var strQuery = "Insert into subscribers SET ?";
    var post = {
        name: subscriber.name,
        email: subscriber.useremail,
        stime: subscriber.stime
    };
    var query = connection.query(strQuery, post, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            sendgrid.logMail(subscriber);
            sendgrid.sendMailSubscriber(subscriber, res);
        }
    });
}

exports.putqExpertAnswer = function (qExpert, res) {
    var strQuery = "UPDATE questionexpert SET ? " +
             " WHERE id=" + qExpert.id;

    var post = { response: qExpert.response,
        expert: qExpert.expert,
        atime: qExpert.atime
    };
    var query = connection.query(strQuery, post, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            res.sendStatus(200);
        }
    });
}

exports.getQuestion = function (id, hostname, res) {
    var strQuery = "SELECT * from questionexpert WHERE id=" + id;

    var query = connection.query(strQuery, function (err, rows, fields) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            console.log(rows[0]);
            var qExpert = new qexpert(rows[0]);
            var variables = {
                question: qExpert.question,
                fullname: qExpert.name,
                email: qExpert.useremail,
                id: id
            };
            res.render('questionexpert.ejs', variables);
        }
    });
}

exports.putCareerInfo = function (profileInfo, res) {
    console.log("Twitter link here is " + profileInfo.twlink);
    var strQuery = "Insert into careers SET ?";
    var post = {
        name: profileInfo.name,
        email: profileInfo.email,
        locale: profileInfo.locale,
        linkedinlink: profileInfo.linkedinlink,
        twlink: profileInfo.twlink,
        timings: profileInfo.timingSchedule,
        fblink: profileInfo.fblink
    };
    var query = connection.query(strQuery, post, function (err, result) {
        if (err) {
            console.log(err);
            res.sendStatus(404).send(err);
        } else {
            res.sendStatus(200);
        }
    });
}