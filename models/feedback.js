function feedback(data) {
    console.log(data);
    this.fullname= String(data.name) || null;

    this.email = String(data.email) || null;
    this.message = String(data.message) || "";
}

module.exports = feedback;