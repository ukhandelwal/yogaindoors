var sendgrid = require('sendgrid')('azure_3329e5dbfbcd3f25b625f3b3c4df4ad9@azure.com', '9kPdIQ0t54W95kk');
var filereader = require('fs');
var EJS = require('ejs');

var payload = {
    from: 'schedulingteam@yogaindoors.com',
    subject: 'Greetings from YogaIndoors'
};

function readfileToString(email, res) {
    var htmlFileString = filereader.readFile("./emailtemplates/demoregistration.html", function (err, data) {
        // var bufferString = data.toString();
        console.log(data.toString());
        var emailParams = new sendgrid.Email(payload);
        emailParams.setHtml(data.toString());
        emailParams.addTo(email);
        //    emailParams.setHtml(bufferString);
        sendgrid.send(emailParams, function (err, json) {
            if (err) {
                console.error("Error " + err);
                res.render('index');
            } else {
                console.log(json);
                res.render('index');
            }

        });
    });
}


exports.sendTeamUserRegMail = function sendTeamUserRegMail(data) {
    var userPayLoad = {
        from: 'schedulingteam@yogaindoors.com',
        subject: 'New user registration'
    };
    var emailParams = new sendgrid.Email(userPayLoad);
    var text = " New user has registered. The mail id of the user is " + data.useremail + " and the preferred timing is " +
    data.time + " " + data.locale;
    emailParams.setText(text);
    emailParams.addTo('swapnajk2001@gmail.com');
    sendgrid.send(emailParams, function (err, json) {
        if (err) {
            console.error("Error " + err);
        } else {
            console.log("Registration Mail sent " + json);
        }

    });
}

exports.sendRegistrationEmail = function sendRegistrationEmail(email, res) {
    readfileToString(email, res);
}

exports.sendQuestionExpert = function sendQuestionExpert(id, hostname, res) {
    var emailParams = new sendgrid.Email(payload);
    var emailText = "You have been asked a question by a user. Please follow the link below to answer";
    emailText += " " + hostname + "/qExpertAnswerForm/" + id;
    emailParams.addTo("swapna@yogaindoors.com");
    emailParams.addTo("uditk2@gmail.com");
    emailParams.setText(emailText);
    sendgrid.send(emailParams, function (err, json) {
        if (err) {
            console.error("Error " + err);
        } else {
            console.log(json);
            res.render('index');
        }
    });
}

function compileEjsHtml(titleString, username) {
    var compiled = EJS.compile(filereader.readFileSync("./emailtemplates/subscription.ejs", 'utf8'));
    var html = compiled({ title: titleString, user: username });
    return html;
}
exports.sendMailSubscriber = function sendMailSubscriber(data, res) {

    var html = compileEjsHtml("Yoga Indoors Subscribed", data.name);
    var userPayLoad = {
        from: 'subscription@yogaindoors.com',
        subject: 'Yoga Indoors Subscribed'
    };
    var emailParams = new sendgrid.Email(userPayLoad);
    emailParams.setHtml(html);
    emailParams.addTo(data.useremail);
    sendgrid.send(emailParams, function (err, json) {
        if (err) {
            console.error("Error " + err);
        } else {
            console.log("Registration Mail sent " + json);
            res.render('index');
        }
    });
}

exports.logMail = function logMail(data) {
    var userPayLoad = {
        from: 'subscription@yogaindoors.com',
        subject: 'New user subscription'
    };
    var emailParams = new sendgrid.Email(userPayLoad);
    var text = " New user subscription  witht email id " + data.useremail + " and name " + data.name;
    " upcoming sessions and new ventures";
    emailParams.setText(text);
    emailParams.addTo('udit@yogaindoors.com');
    sendgrid.send(emailParams, function (err, json) {
        if (err) {
            console.error("Error " + err);
        } else {
            console.log("New user subscription done " + json);
        }
    });
}